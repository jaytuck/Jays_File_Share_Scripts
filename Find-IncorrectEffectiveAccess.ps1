param(
        [String][Parameter(Mandatory=$true)]$Path,
        [String][Parameter(Mandatory=$true)]$Account,
        [Int32]$Depth = 999,
        [String[]][ValidateSet("None","ReadData","ListDirectory","CreateFiles","CreateDirectories","AppendData","ReadExtendedAttributes","WriteExtendedAttributes","Traverse","ExecuteFile","DeleteSubdirectoriesAndFiles","ReadAttributes","WriteAttributes","Write","Delete","ReadPermissions","Read","ReadAndExecute","Modify","ChangePermissions","TakeOwnership","Synchronize","FullControl","GenericAll","GenericExecute","GenericWrite","GenericRead")]$ignore_access_types = (New-Object System.Collections.ArrayList)
)
Import-Module NTFSSecurity


Function Find-IncorrectEffectiveAccess {
    param(
        [String][Parameter(Mandatory=$true)]$Path,
        [String][Parameter(Mandatory=$true)]$Account,
        [Int32]$Depth = 999,
        [String[]][ValidateSet("None","ReadData","ListDirectory","CreateFiles","CreateDirectories","AppendData","ReadExtendedAttributes","WriteExtendedAttributes","Traverse","ExecuteFile","DeleteSubdirectoriesAndFiles","ReadAttributes","WriteAttributes","Write","Delete","ReadPermissions","Read","ReadAndExecute","Modify","ChangePermissions","TakeOwnership","Synchronize","FullControl","GenericAll","GenericExecute","GenericWrite","GenericRead")]$ignore_access_types
    )

    Write-Verbose "Getting access for path $Path"
    $item = Get-Item -Path $Path
    
    $item | Get-NTFSEffectiveAccess -Account $Account | Where-Object {$_.AccessRights -notin $ignore_access_types}

    Write-Verbose "Depth is $Depth and current item is $($item.mode)"
    if($Depth -ge 0 -and $item.Mode -like 'd*') {
        Get-ChildItem -Path $Path | foreach {
            Find-IncorrectEffectiveAccess -Path $_.FullName -Account $Account -Depth ($Depth - 1) -ignore_access_types $ignore_access_types
        }
    }
}

Find-IncorrectEffectiveAccess -Path $Path -Account $Account -Depth $Depth -ignore_access_types $ignore_access_types